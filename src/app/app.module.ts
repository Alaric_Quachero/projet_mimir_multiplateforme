import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Http } from '@angular/http';
import { TranslateModule, TranslateStaticLoader, TranslateLoader} from 'ng2-translate/ng2-translate';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { RiddlesListPage } from '../pages/riddles-list/riddles-list';
import { ItemDetailsPage } from '../pages/item-details/item-details';
import { ContactPage } from '../pages/contact/contact';
import { ContactFormPage } from '../pages/contact/contact-form/contact-form';
import { SettingPage } from '../pages/setting/setting';
import { FiftyPage } from '../pages/riddles/fifty/fifty';
import { LightPage } from '../pages/riddles/light/light';
import { TicTacPage } from '../pages/riddles/tictac/tictac';
import { WhisperPage }from '../pages/riddles/whisper/whisper';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    RiddlesListPage,
    ItemDetailsPage,
    ContactPage,
    ContactFormPage,
    SettingPage,
    FiftyPage,
    LightPage,
    TicTacPage,
    WhisperPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [Http]
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    RiddlesListPage,
    ItemDetailsPage,
    ContactPage,
    ContactFormPage,
    SettingPage,
    FiftyPage,
    LightPage,
    TicTacPage,
    WhisperPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}

export function createTranslateLoader(http: Http) {
  return new TranslateStaticLoader(http, 'assets/i18n', '.json');
}

