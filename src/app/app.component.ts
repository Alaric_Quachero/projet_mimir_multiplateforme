import { Component, ViewChild } from '@angular/core';

import { Platform, MenuController, Nav } from 'ionic-angular';

import { StatusBar, Splashscreen } from 'ionic-native';

import { TranslateService } from 'ng2-translate';

import { AppPreferences } from '@ionic-native/app-preferences';

import { HomePage } from '../pages/home/home';
import { RiddlesListPage } from '../pages/riddles-list/riddles-list';
import { ContactPage } from '../pages/contact/contact';
import { SettingPage } from '../pages/setting/setting';

@Component({
  templateUrl: 'app.html',
  providers: [AppPreferences]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make HelloIonicPage the root (or first) page
  rootPage: any = HomePage;
  pages: Array<{title: string, component: any}>;

  constructor(
    public platform: Platform,
    public menu: MenuController,
    public translate: TranslateService,
    private appPreferences: AppPreferences,
  ) {
    this.initializeApp();

    // set translation
    //TODO Marion: Utiliser le default du téléphone
    translate.setDefaultLang('en');
    this.appPreferences.fetch('language').then(
        (success) => { 
          translate.use(success);
          if(success == undefined || success == ''){
            translate.use('en');
          }
         }, 
        (fail) => { translate.use('en'); });

    // set our app's pages
    translate.onLangChange.subscribe(() => {
        setTimeout(() => {
      this.pages = [];
      translate.get('Homepage').subscribe(res => {
        this.pages.push({title: res, component: HomePage});
       });
      translate.get('Riddles list').subscribe(res => {
       this.pages.push({title: res, component: RiddlesListPage});
       });
      translate.get('Contact').subscribe(res => {
       this.pages.push({title: res, component: ContactPage});
       });
      translate.get('Settings').subscribe(res => {
       this.pages.push({title: res, component: SettingPage});
       });
        }, 500);
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.push(page.component);
  }
}
