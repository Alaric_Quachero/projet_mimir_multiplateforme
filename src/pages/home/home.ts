import { AppPreferences } from '@ionic-native/app-preferences';
import { Component } from '@angular/core';
import { NativeAudio } from '@ionic-native/native-audio';

import { NavController, Platform, LoadingController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';

import { RiddleModel } from '../../model/riddle';
import { RiddlesListPage } from '../riddles-list/riddles-list';
import { ContactPage } from '../contact/contact';
import { SettingPage } from '../setting/setting';
import { FiftyPage } from '../riddles/fifty/fifty';
import { TicTacPage } from '../riddles/tictac/tictac';
import { LightPage } from '../riddles/light/light';
import { WhisperPage } from '../riddles/whisper/whisper';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [NativeAudio]
})
export class HomePage {

  private Pages = {
    'FiftyPage' : FiftyPage,
    'TicTacPage' : TicTacPage,
    'LightPage' : LightPage,
    'WhisperPage' : WhisperPage
  }

  play = this.fetchBackground;
  riddlesList = RiddlesListPage;
  contact = ContactPage;
  settings = SettingPage;
  private bgImage;
  private riddleModel;
  private displayFrog;

  constructor(public navCtrl: NavController, public platform: Platform, private appPreferences: AppPreferences, private nativeAudio: NativeAudio, public translate: TranslateService, public loadingCtrl: LoadingController){

		this.riddleModel = new RiddleModel(platform, translate);
		this.displayFrog = false;

	}

	ionViewDidLoad(){
		this.nativeAudio.preloadComplex('frogSound', 'assets/sound/frogs.mp3', 1, 1, 0).then((success) => {},(fail) => {});
		this.nativeAudio.preloadComplex('dayDreamSound', 'assets/sound/daydream.mp3', 1, 1, 0).then((success) => {},(fail) => {});
		this.fetchBackground(); 
	}

	ionViewWillEnter(){
		this.fetchBackground();
	}

  ionViewDidEnter(){
     this.playFrogSound(this.bgImage);
  }

  ionViewDidLeave(){
  	this.nativeAudio.stop('frogSound').then((success) => {},(fail) => {});
  }

	playFrogSound(anImage:string){
	  if(anImage == 'assets/images/background_home_night.png'){
			this.nativeAudio.loop('frogSound').then((success) => {},(fail) => {});
		}
		else {
			this.nativeAudio.stop('frogSound').then((success) => {},(fail) => {});
		}
	}

	fetchBackground(){
		this.appPreferences.fetch('theme').then(
        (success) => { 
        	this.bgImage = success
        	if(this.bgImage == undefined || this.bgImage == ''){
						this.bgImage = 'assets/images/background_home.png';
        	}
    			if(this.bgImage == 'assets/images/background_home_night.png') {
						this.displayFrog = true;
					}
					else{
						this.displayFrog = false;
					}
        },
        (fail) => { 
        	this.bgImage = 'assets/images/background_home.png';
		});
	}

	playGame() {
		//this.navCtrl.push();
		this.riddleModel.getFirstUnlockRiddle();
		do {
			this.presentLoadingText()
		} while (this.riddleModel.riddle['namePage'] == "");
	}

	presentLoadingText() {
  let loading = this.loadingCtrl.create({
    spinner: 'crescent',
    content: 
		'    	<div class="custom-spinner-container">'+
		'        <div class="custom-spinner-box"></div>'+
		'      </div>'
  });

  loading.present();

  setTimeout(() => {
    this.navCtrl.push(this.Pages[this.riddleModel.riddle['namePage']]);
  }, 2000);

  setTimeout(() => {
    loading.dismiss();
  }, 1000);
}

frogEvent(){
		this.nativeAudio.stop('frogSound').then((success) => {},(fail) => {});
		this.nativeAudio.play('dayDreamSound').then((success) => {},(fail) => {});
		setTimeout(() => {
						this.nativeAudio.stop('dayDreamSound').then((success) => {},(fail) => {});
	    			this.nativeAudio.loop('frogSound').then((success) => {},(fail) => {});
	  }, 2000);
	}
		
}
