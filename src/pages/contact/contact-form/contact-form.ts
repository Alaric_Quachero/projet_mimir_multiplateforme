import { Component } from '@angular/core';

import { EmailComposer } from 'ionic-native';

import { NavParams } from 'ionic-angular';

import { ModalController, ViewController  } from 'ionic-angular';

@Component({
  selector: 'page-contact-form',
  templateUrl: 'contact-form.html'
})
export class ContactFormPage {

  constructor(public viewCtrl: ViewController, public modalCtrl: ModalController, private navParams: NavParams) {

  }

  mail = {
  	subject: [''],
  	message: ['']
  }
  formMail() {
    let email = {
  	  to: this.navParams.get("address"),
  	  subject: this.mail.subject.toString(),
  	  body: this.mail.message.toString()
	  };
	  EmailComposer.open(email);
    this.viewCtrl.dismiss();
  }

  closemodal(){
  	this.viewCtrl.dismiss();
  }
}