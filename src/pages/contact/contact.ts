import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { ModalController } from 'ionic-angular';

import { ContactFormPage } from './contact-form/contact-form';


@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  constructor(public navCtrl: NavController, public modalCtrl: ModalController) {

  }

  openModal(address) {
	let modal = this.modalCtrl.create(ContactFormPage, {"address": address});
	modal.present();
  }

}
