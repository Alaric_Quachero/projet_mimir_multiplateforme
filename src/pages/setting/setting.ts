import { Component } from '@angular/core';
import { TranslateService } from 'ng2-translate';

import { AppPreferences } from '@ionic-native/app-preferences';

@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html'
})
export class SettingPage {
	pages: Array<{title: string, component: any}>;

  constructor(public translateService: TranslateService, private appPreferences: AppPreferences) {
  }

  translateTo(language: string) {
		this.translateService.use(language);
		this.appPreferences.store('language', language);
	}

	useTheme(theme: string){
		this.appPreferences.store('theme', theme);
	}

}
