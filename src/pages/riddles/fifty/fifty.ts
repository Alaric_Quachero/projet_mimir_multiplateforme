import { Component } from '@angular/core';
import { AlertController, NavController, Platform } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';
import { RiddleModel } from '../../../model/riddle';

import { HomePage } from '../../home/home';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LightPage } from '../light/light';

@Component({
  selector: 'page-fifty-ionic',
  templateUrl: 'fifty.html'
})
export class FiftyPage {
	private todo : FormGroup;
  private riddleModel;

constructor(public navCtrl : NavController, private formBuilder: FormBuilder, private alertCtrl: AlertController, private platform: Platform, public translate: TranslateService) {

    this.todo = this.formBuilder.group({
          reponse: ['', Validators.required],
    });

    this.riddleModel = new RiddleModel(platform, translate); 
  }

  changeImageBoutton() {
  	if (window.getComputedStyle(document.getElementById('tonDiv1')).display=='none'){
		document.getElementById("tonDiv1").style.display="block";

		document.getElementById("tonDiv2").style.display="none";
   }
	else {
		document.getElementById("tonDiv1").style.display="none";
		document.getElementById("tonDiv2").style.display="block";
     }
  }

  test  = {
    essais: ['']
  }
 
  reponse(){
    
  console.log("test affichage " + this.test.essais.toString());
    
  if(this.test.essais.toString().toUpperCase()== "Share".toUpperCase()){
  let alert = this.alertCtrl.create({
    title: 'Félicitation !',
    subTitle: 'Votre réponse est correcte',
    buttons: [{text:'Accueil', handler: () => {                    
        this.navCtrl.setRoot(HomePage);
    }},{text:'Engime suivante', handler: () => {
       
        this.navCtrl.push(LightPage);
    }}]
  });
  alert.present();
  this.riddleModel.setIsUnlockTrueByNumber(3);
  	}
  	else{
      let alert = this.alertCtrl.create({
        title: 'Zut !',
        subTitle: 'Votre réponse n\'est pas correcte',
        buttons: [{text:'Accueil', handler: () => {

          this.navCtrl.setRoot(HomePage);
        }},{text:'Recommencer'}]

      });
      alert.present();
  	}
  }
}
