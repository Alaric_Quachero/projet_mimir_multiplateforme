import { Component } from '@angular/core';
import { AlertController, NavController, Platform } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';
import { RiddleModel } from '../../../model/riddle';

import { HomePage } from '../../home/home';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'page-whisper-ionic',
  templateUrl: 'whisper.html'
})
export class WhisperPage {
  private todo : FormGroup;
  private riddleModel;

  constructor(public navCtrl : NavController, private formBuilder: FormBuilder, private alertCtrl: AlertController, private platform: Platform, public translate: TranslateService) {

    this.todo = this.formBuilder.group({
      reponse: ['', Validators.required],
    });

    this.riddleModel = new RiddleModel(platform, translate); 
  }

  whisper  = {
    answer: ['']
  }

  reponse(){
    if(this.whisper.answer.toString().toUpperCase()== "daydream".toUpperCase()){
      let alert = this.alertCtrl.create({
        title: 'Félicitation !',
        subTitle: 'Votre réponse est correcte',
        buttons: [{text:'Accueil', handler: () => {                    
          this.navCtrl.setRoot(HomePage);
        }},{text:'Engime suivante', handler: () => {

          this.navCtrl.push(HomePage);
        }}]
      });
      alert.present();
    }
    else{
      let alert = this.alertCtrl.create({
        title: 'Zut !',
        subTitle: 'Votre réponse n\'est pas correcte',
        buttons: [{text:'Accueil', handler: () => {

          this.navCtrl.setRoot(HomePage);
        }},{text:'Recommencer'}]
      });
      alert.present();
    }
  }

}
